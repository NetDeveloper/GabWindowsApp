﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace App1
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            JsonObjects.User user = new JsonObjects.User();
            user.name = "Dylan Snyder";
            user.username = "Deathslyce";
            user.picture_url = "https://i.redd.it/8jtx1zbwlb5y.jpg";
            JsonObjects.Post pli = new JsonObjects.Post();
            pli.id = 1;
            pli.user = user;
            pli.is_locked = false;
            pli.is_premium = false;
            pli.is_quote = false;
            pli.is_reply = false;
            pli.liked = false;
            pli.like_count = 10;
            pli.nsfw = false;
            pli.reposted = false;
            pli.score = 10;
            pli.edited = false;
            pli.dislike_count = 1;
            pli.disliked = false;
            pli.created_at = "3/12/14";
            pli.category = 1;
            pli.bookmarked = false;
            pli.body = "This is a post. Congrats";
            PostListItem pl = new PostListItem(pli);
            
            PostsListView.Items.Add(pl);
            Uset_Controls.ProfilesForList pf = new Uset_Controls.ProfilesForList(user);
            SearchUserList.Items.Add(pf);
            Uset_Controls.ProfilesForList pf1 = new Uset_Controls.ProfilesForList(user);
            SearchUserList.Items.Add(pf1);
            Uset_Controls.ProfilesForList pf2 = new Uset_Controls.ProfilesForList(user);
            SearchUserList.Items.Add(pf2);
            Uset_Controls.ProfilesForList pf3 = new Uset_Controls.ProfilesForList(user);
            SearchUserList.Items.Add(pf3);
            Uset_Controls.ProfilesForList pf4 = new Uset_Controls.ProfilesForList(user);
            SearchUserList.Items.Add(pf4);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(WriteAPostPage));
        }

        private void Pivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PivotItem pi = (PivotItem)e.AddedItems[0];

            {

                if ((string)pi.Header == "Images/Home.png")
                {
                    pi.Header = "Images/HomePressed.png";
            }
                else if ((string)pi.Header == "Images/Search.png")
                {
                    pi.Header = "Images/SearchPressed.png";
                }
                else if ((string)pi.Header == "Images/Categories.png")
                {
                    pi.Header = "Images/CategoriesPressed.png";
                }
                else if ((string)pi.Header == "Images/News.png")
                {
                    pi.Header = "Images/NewsPressed.png";
                }
                else if ((string)pi.Header == "Images/Trending.png")
                {
                    pi.Header = "Images/TrendingPressed.png";
                }
            }
            try
            {
                PivotItem pi1 = (PivotItem)e.RemovedItems[0];
            
                    if ((string)pi1.Header == "Images/HomePressed.png")
                {
                    pi1.Header = "Images/Home.png";
                }
               else if ((string)pi1.Header == "Images/SearchPressed.png")
                {
                    pi1.Header = "Images/Search.png";
                }
               else if ((string)pi1.Header == "Images/CategoriesPressed.png")
                {
                    pi1.Header = "Images/Categories.png";
                }
                else if ((string)pi1.Header == "Images/NewsPressed.png")
                {
                    pi1.Header = "Images/News.png";
                }
                else if ((string)pi1.Header == "Images/TrendingPressed.png")
                {
                    pi1.Header = "Images/Trending.png";
                }
            }
            catch (Exception ex)
            {
                Debug.Write(ex.Message);
            }
         
        }

        private void notificationButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(NotificationPage));
        }
    }
}
