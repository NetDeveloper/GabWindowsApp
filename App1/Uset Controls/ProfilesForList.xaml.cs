﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace App1.Uset_Controls
{
    public sealed partial class ProfilesForList : UserControl
    {
        public JsonObjects.User user;
        public ProfilesForList(JsonObjects.User user)
        {
            this.InitializeComponent();
            profilePicImage.ImageSource = new BitmapImage(new Uri(user.picture_url));
            NameText.Text = user.name;
        }
    }
}
