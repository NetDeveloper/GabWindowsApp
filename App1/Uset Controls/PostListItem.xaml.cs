﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace App1
{
    public sealed partial class PostListItem : UserControl
    {


       public JsonObjects.Post post = new JsonObjects.Post();
        public PostListItem(JsonObjects.Post post)
        {
            this.post = post;
            this.InitializeComponent();
            bodyText.Text = post.body;
            nameText.Text = post.user.name;
            usernameText.Text = "@" + post.user.username;
            profilePicImage.ImageSource = new BitmapImage(new Uri(post.user.picture_url));
        }
        public PostListItem()
        {
            this.InitializeComponent();
        }
    }
}
