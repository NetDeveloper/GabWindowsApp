﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1.JsonObjects
{
    public class Post
    {
        public int id { get; set; }
        public string created_at { get; set; }
        public string revised_at { get; set; }
        public bool edited { get; set; }
        public string body { get; set; }
        public bool liked { get; set; }
        public bool disliked { get; set; }
        public bool bookmarked { get; set; }
        public bool reposted { get; set; }
        public int score { get; set; }
        public int like_count { get; set; }
        public int dislike_count { get; set; }
        public bool is_quote { get; set; }
        public bool is_reply { get; set; }
        public int category { get; set; }
        public string language { get; set; }
        public bool nsfw { get; set; }
        public bool is_premium { get; set; }
        public bool is_locked { get; set; }
        public User user { get; set; }
    }
}
