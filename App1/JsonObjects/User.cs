﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1.JsonObjects
{
    public class User
    {
        public int id { get; set; }
        public string name { get; set; }
        public string username { get; set; }
        public string picture_url { get; set; }
        public string picture_url_full { get; set; }
        public string cover_url { get; set; }
        public bool verified { get; set; }
        public bool is_investor { get; set; }
        public bool is_pro { get; set; }
        public bool is_private { get; set; }
        public bool is_accessable { get; set; }
        public bool is_premium { get; set; }
        public int follower_count { get; set; }
        public int following_count { get; set; }
        public int post_count { get; set; }
        public bool following { get; set; }
        public bool followed { get; set; }
        public bool follow_pending { get; set; }
        public bool subscribing { get; set; }
        public bool is_tippable { get; set; }
        public double premium_price { get; set; }
        public string bio { get; set; }
        public int score { get; set; }
        public int video_count { get; set; }
    }

}
