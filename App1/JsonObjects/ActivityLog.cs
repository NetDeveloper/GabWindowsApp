﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1.JsonObjects
{
    class ActivityLog
    {
        public string id { get; set; }
        public string created_at { get; set; }
        public User actuser { get; set; }
        public Post post { get; set; }
        public string type { get; set; }
    }
}
